SENTRY_AUTH_TOKEN=865e59e0da004b1fabb2e7e5d9ea7c6550df072ec1ff4a589036ab6592bf0494
SENTRY_ORG=isteer
SENTRY_PROJECT=python-backend-monitoring
VERSION=myapp@1.0.0

deploy: install create_release associate_commits run_django

install:
	pip install -r ./requirements.txt

create_release:
	sentry-cli releases -o $(SENTRY_ORG) new -p $(SENTRY_PROJECT) $(VERSION)

associate_commits:
	sentry-cli releases -o $(SENTRY_ORG) -p $(SENTRY_PROJECT) \
		set-commits $(VERSION) --auto

run_django:
	VERSION=$(VERSION) python manage.py runserver
